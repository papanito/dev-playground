# 12. Generate Passwords Automatically
#  
# Source: https://www.tecmint.com/python-automation-scripts/
#
# Creating strong, unique passwords is essential for security, and Python can help automate this process using the random module.
# 
# Below is a simple script that generates random passwords of a specified length, incorporating letters, digits, and special characters to enhance security.

import random
import asyncio
import string

async def generate_password(length=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for _ in range(length))
    return password

async def generate_multiple_passwords(n, length=12):
    tasks = [generate_password(length) for _ in range(n)]
    passwords = await asyncio.gather(*tasks)
    print(passwords)

asyncio.run(generate_multiple_passwords(5))
