# 18. Creating Daily Reminders
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# Setting daily reminders is easy with the time module, which will remind you to drink water every 2 hours:

import time

def water_reminder():
    while True:
        print("Time to drink water!")
        time.sleep(7200)  # Remind every 2 hours

water_reminder()
