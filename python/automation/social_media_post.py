# 7. Automating Social Media Posts
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
#  If you manage social media accounts, then you can automate posting by using the libraries like Tweepy (for Twitter) and Instagram-API (for Instagram) allow you to post automatically.
#
# Below is an example using the Tweepy library to post a tweet:
# This script posts a tweet with the message “Hello, world!” to your Twitter account.

import tweepy

def tweet(message):
    consumer_key = 'your_consumer_key'
    consumer_secret = 'your_consumer_secret'
    access_token = 'your_access_token'
    access_token_secret = 'your_access_token_secret'

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    api.update_status(message)
    print("Tweet sent successfully!")

tweet("Hello, world!")

