# 2. Backing Up Files Automatically
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# We all know how important it is to back up files regularly, and this task can be easily automated using Python’s shutil module.
#
# This script will copy all files from one directory to another for backup purposes:
# source = '/path/to/source/directory' destination = '/path/to/destination/directory' backup_files(source, destination)
#
# You can schedule this script to run daily using task-scheduling tools like cron (Linux) or Task Scheduler (Windows).

import shutil
import os

def backup_files(src_dir, dest_dir):
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    for file in os.listdir(src_dir):
        full_file_name = os.path.join(src_dir, file)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, dest_dir)
            print(f"Backed up {file} to {dest_dir}")

