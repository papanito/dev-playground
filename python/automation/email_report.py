# 4. Automating Email Reports
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# If you’re required to send email reports regularly, you can automate it using smtplib library, which allows you to send emails from a Gmail account easily:
#
# This script will send a simple email with a subject and body to a specified recipient. Make sure to enable less secure apps in Gmail if you use this method.
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_email(subject, body, to_email):
    sender_email = 'youremail@gmail.com'
    sender_password = 'yourpassword'
    receiver_email = to_email

    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = receiver_email
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, receiver_email, msg.as_string())
        server.quit()
        print("Email sent successfully!")
    except Exception as e:
        print(f"Failed to send email: {e}")

subject = 'Monthly Report'
body = 'Here is the monthly report.'
send_email(subject, body, 'receiver@example.com')
