# 17. Automating Data Backup to Cloud
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# Automating backups to cloud services like Google Drive is made possible with Python using libraries such as pydrive.
# file = '/path/to/your/file.txt' backup_to_google_drive(file)

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

def backup_to_google_drive(file_path):
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()
    drive = GoogleDrive(gauth)
    file = drive.CreateFile({'title': 'backup_file.txt'})
    file.Upload()
    print("Backup uploaded successfully!")

