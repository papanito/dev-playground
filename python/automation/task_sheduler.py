# 5. Task Scheduler (Task Automation)
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# Scheduling tasks can be done easily using the schedule library, which allows you to automate tasks like sending an email or running a backup script at specific times:
#
# This script will keep running and trigger tasks at the specified time, in this case, 10:00 AM every day.

import schedule
import time

def job():
    print("Running scheduled task!")

# Schedule the task to run every day at 10:00 AM
schedule.every().day.at("10:00").do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
