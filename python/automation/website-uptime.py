# 9. Monitoring Website Uptime
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
#  Python can be used to automate monitoring of website uptime using the requests library, that can periodically check if a website is online or not:
# url = 'https://example.com' while True: check_website(url) time.sleep(3600) # Check every hour
#
#  This script checks if the website is online and prints the status code.
import requests
import time

def check_website(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            print(f"Website {url} is up!")
        else:
            print(f"Website {url} returned a status code {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"Error checking website {url}: {e}")
