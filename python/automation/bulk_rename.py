# 1. Renaming Files in Bulk
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# Renaming files one by one can be a time-consuming task, but with Python, you can easily automate this by using the os module.
# Here’s a simple script that renames multiple files in a folder based on a given pattern:
# This script searches for files containing old_name_part in their names and replaces it with new_name_part.
# folder = '/path/to/your/folder' bulk_rename(folder, 'old_part', 'new_part')
import os

def bulk_rename(folder_path, old_name_part, new_name_part):
    for filename in os.listdir(folder_path):
        if old_name_part in filename:
            new_filename = filename.replace(old_name_part, new_name_part)
            os.rename(os.path.join(folder_path, filename), os.path.join(folder_path, new_filename))
            print(f"Renamed {filename} to {new_filename}")


