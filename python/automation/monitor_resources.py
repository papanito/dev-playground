# 15. Monitor System Resources
#
# Source: https://www.tecmint.com/python-automation-scripts/
#
# If you’re a system administrator, you can use Python to monitor your system’s resources like CPU and memory usage, with the help of psutil library.

import psutil

def monitor_resources():
    cpu_usage = psutil.cpu_percent(interval=1)
    memory_usage = psutil.virtual_memory().percent
    print(f"CPU Usage: {cpu_usage}%")
    print(f"Memory Usage: {memory_usage}%")

monitor_resources()
